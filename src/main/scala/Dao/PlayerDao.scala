package Dao

import Domain.{Player, PlayerEntity}
import Service.AkkaHttpMicroservice
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter, BSONObjectID}

/**
  * Created by wysocki on 28.5.2016.
  */
trait PlayerDao
{
	lazy val collection: BSONCollection = AkkaHttpMicroservice.db("players")

	implicit def toPlayerEntity(player: Player) = PlayerEntity(login = player.login, gold = player.gold)

	implicit object PlayerEntityBSONReader extends BSONDocumentReader[PlayerEntity]
	{
		def read(doc: BSONDocument): PlayerEntity =
		{
			PlayerEntity(
				id = doc.getAs[BSONObjectID]("_id").get,
				login = doc.getAs[String]("login").get,
				gold = doc.getAs[Int]("gold").get
			)
		}
	}

	implicit object PlayerEntityBSONWriter extends BSONDocumentWriter[PlayerEntity]
	{
		def write(playerEntity: PlayerEntity): BSONDocument =
		{
			BSONDocument(
				"_id" -> playerEntity.id,
				"login" -> playerEntity.login,
				"gold" -> playerEntity.gold
			)
		}
	}

}
