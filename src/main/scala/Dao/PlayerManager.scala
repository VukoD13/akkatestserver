package Dao

import Domain.{Player, PlayerEntity}
import Service.Protocols
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}
import spray.json._

/**
  * Created by wysocki on 28.5.2016.
  */
class PlayerManager extends PlayerDao with Protocols
{

	def save(playerEntity: PlayerEntity): Future[String] =
	{
		val f = collection.insert(playerEntity)

		f.onComplete
		{
			case Failure(e) =>
				throw e
			case Success(writeResult) =>
				println(s"successfully inserted document with result: $writeResult")
		}

		f.map
		{ t =>
			println("message: " + t.message)
			t.ok match
			{
				case true =>
					"SUCCESS"
				case false =>
					"FAIL"
			}
		}
	}

	def get(login: String): Future[String] =
	{
		val query = BSONDocument("login" -> login)

		val p = collection.find(query).one[PlayerEntity].map
		{ p =>
			println(p.get)
			new Player(p.get.login, p.get.gold).toJson.toString()
		}
			.recover //onException -> on failure
		{
			case ex: NoSuchElementException =>
				println(s"$ex\nNie znaleziono playera w bazie")
				"FAIL"
			case ex: Exception =>
				println(ex)
				"FAIL"
		}
		p
	}
}
