package Domain

import reactivemongo.bson.BSONObjectID

/**
  * Created by wysocki on 28.5.2016.
  */

case class Player(login: String, gold: Int)

case class PlayerEntity(id: BSONObjectID = BSONObjectID.generate, login: String, gold: Int)
