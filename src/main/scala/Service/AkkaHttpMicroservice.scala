package Service

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import reactivemongo.api.{MongoConnection, MongoDriver}

import scala.concurrent.ExecutionContext.Implicits.global

object AkkaHttpMicroservice extends App with Service
{
	override implicit val system = ActorSystem()
	override implicit val materializer = ActorMaterializer()

	val config = ConfigFactory.load()

	val uri = config.getString("mongodb.uri")
	val database = config.getString("mongodb.db")
	//import collection.JavaConversions._
	//val servers = config.getStringList("mongodb.servers").toList

	val driver = new MongoDriver
	var connection: MongoConnection =
		MongoConnection.parseURI(uri).map
		{ parsedUri =>
			driver.connection(parsedUri)
		}.get

	val db = connection(database)

	Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
