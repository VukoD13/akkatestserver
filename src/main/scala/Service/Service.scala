package Service

import Dao.PlayerManager
import Domain.{PlayerEntity, Player}
import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives
import akka.stream.Materializer
import spray.json.DefaultJsonProtocol

/**
  * Created by wysocki on 28.5.2016.
  */

trait Protocols extends SprayJsonSupport with DefaultJsonProtocol
{
	implicit val playerFormat = jsonFormat2(Player.apply)
}

trait Service extends Directives with Protocols //musza byc protokoly tez do entity(as[json
{
	implicit val system: ActorSystem
	implicit val materializer: Materializer

	val playerManager = new PlayerManager

	val routes =
	{
		pathPrefix("player")
		{
			(path("add") & get & parameters('login)) //http://localhost:9000/player/add?login=xdd or http://localhost:9000/player/add?params&login=xdd opcjonalne 'opt.?
			{
				//zamiast parametru mozna uzyc path(Segment) i wywolac np: http://localhost:9000/player/add/xdd
				login =>
					val res = playerManager.save(playerManager.toPlayerEntity(new Player(login, 0)))
					complete(res)
			} ~
				(path("get") & get & parameters('login))
				{
					login =>
						complete(playerManager.get(login))
				} ~
				(path("add") & post & entity(as[Player])) //dobre narzedzie w chrome: POSTMAN
			//Headers -> Content-Type application/json
			//Body, np {"login":"op2", "gold":50} w formie raw
				{ player =>
					val res = playerManager.save(playerManager.toPlayerEntity(player))
					println(res)
					complete(res)
				}
		}
	}
}
